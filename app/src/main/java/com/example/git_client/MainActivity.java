package com.example.git_client;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.git_client.Models.GitUser;
import com.example.git_client.api.ImplGitHub;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<GitUser> mUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUsers = new ArrayList<>();

    }
    @Override
    protected void onResume(){
        super.onResume();

        GetUsersTask getUsersTask = new GetUsersTask();
        getUsersTask.execute();

    }

    private  class GetUsersTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute(){
            findViewById(R.id.activity_main_progress_bar).setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            mUsers = ImplGitHub.getInstance().getUsers();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            findViewById(R.id.activity_main_progress_bar).setVisibility(View.GONE);
        }
    }


}
