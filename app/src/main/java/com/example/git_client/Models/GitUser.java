package com.example.git_client.Models;

public class GitUser {

    private String mLogin;
    private int mId;

    public static Builder createBuilder(){
        return new GitUser().new Builder();
    }

    public class Builder {
        private Builder(){

        }
        public Builder init(String login, int id){
            mLogin = login;
            mId = id;
            return this;
        }
        public GitUser build(){
            GitUser.this.mId = mId;
            GitUser.this.mLogin = mLogin;
            return GitUser.this;
        }

    }
}
