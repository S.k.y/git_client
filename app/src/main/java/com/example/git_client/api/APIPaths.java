package com.example.git_client.api;

public final class APIPaths {
    public static String BASE_URL = "https://api.github.com/";
    public static String USERS = "users";
}
