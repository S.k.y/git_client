package com.example.git_client.api;


import com.example.git_client.Models.GitUser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public final class ImplGitHub implements BaseGitAPI {
    private static ImplGitHub mInstance;
    public static BaseGitAPI getInstance(){
        if(mInstance == null){
            mInstance = new ImplGitHub();
        }
        return mInstance;
    }


    public ImplGitHub(){

    }

    @Override
    public ArrayList<GitUser> getUsers() {

        ArrayList<GitUser> data = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder requestString = new StringBuilder(APIPaths.BASE_URL);
        requestString.append(APIPaths.USERS);
        try {
            URL url = new URL(requestString.toString());
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();

            if(responseCode == APICodes.SUCCESS) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                data = parseResponseUsers(bufferedReader);
            }

        } catch (ParseException e1) {
            e1.printStackTrace();
        } catch (ProtocolException e1) {
            e1.printStackTrace();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return data;
    }


    private ArrayList<GitUser> parseResponseUsers(BufferedReader reader) throws IOException, ParseException {
        ArrayList<GitUser> data = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = (JSONArray) jsonParser.parse(reader);
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            String login = (String) jsonObject.get("login");
            //int id = (int) jsonObject.get("id");
            int id = 1;

            GitUser user = GitUser.createBuilder()
                    .init(login, id)
                    .build();
            data.add(user);
        }
        return data;
    }
}
