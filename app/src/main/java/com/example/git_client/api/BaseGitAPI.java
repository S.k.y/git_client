package com.example.git_client.api;

import com.example.git_client.Models.GitUser;

import java.util.ArrayList;

public interface BaseGitAPI {

    ArrayList<GitUser> getUsers();
}
